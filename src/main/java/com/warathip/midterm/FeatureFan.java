package com.warathip.midterm;

public class FeatureFan {
    double power;
    double bladefan;
    double guarantee;
    public FeatureFan(double power, double bladefan , double guarantee) {
        this.power = power;
        this.bladefan = bladefan;
        this.guarantee = guarantee;
    }
    boolean on(String use) {
        return true;
    }
    boolean off(String use) {
        return true;
    }
    void print() {
        System.out.println("Power : "+power+" "+"watt");
        System.out.println("Blade Fan : "+bladefan+" "+"blade");
        System.out.println("Guarantee : "+guarantee+" "+"year");
    }
}
